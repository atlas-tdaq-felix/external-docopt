
# Docopt 0.6.3

docopt.cpp from https://github.com/docopt/docopt.cpp

## NOTE: CMakelists.txt changed version number from 0.6.2 (wrongly) to 0.6.3
## NOTE: as of 0.6.3 changed line 284 stol(str, &pos) in docopt_value.h toLong into stol(str, &pos, 0);
## NOTE: docopt_value.h line 70, moved kindAsString into public to be called as docopt::value::kindAsString(Kind)

## Instructions

* Use CMake
* Set install directory to the path in the external git repository using ccmake
* Build and install (make && make install)
* symbolic link to  'lib64' from 'lib'

example:

mkdir build
cd build
cmake -D CMAKE_INSTALL_PREFIX=`pwd`/../../0.6.3/x86_64-centos9-gcc11-opt -D CMAKE_CXX_STANDARD=17 ..
make
make install
ln -s lib64 lib

# Docopt 0.6.2.4

docopt.cpp from https://github.com/docopt/docopt.cpp

## NOTE: as of 0.6.2.4 changed line 283 stol in docopt_value.h toLong into stol(str, &pos, 0);
## NOTE: as of gcc11 include <stdexcept> in docopt_value.h

## Instructions

* Use CMake
* Set install directory to the path in the external git repository using ccmake
* Build and install (make && make install)
* Rename 'lib64' to 'lib'

example:

mkdir build
cd build
cmake -D CMAKE_INSTALL_PREFIX=`pwd`/../../0.6.2.4/x86_64-centos7-gcc12-opt -D CMAKE_CXX_STANDARD=17 ..
make
make install
rename lib64 to lib
